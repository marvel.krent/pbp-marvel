from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages


# Create your views here.
@login_required(login_url = "/admin/login/")
def index (request):
    friends = Friend.objects.all() 
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)

@login_required(login_url = "/admin/login/")
def add_friend (request):
    form = FriendForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4/')
    else: 
        return render(request, "lab3_form.html", {'form':form})


