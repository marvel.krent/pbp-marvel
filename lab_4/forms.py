from django import forms
from django.forms import fields, widgets
from lab_2.models import Note

class NoteForm (forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        labels = {
            'frm' : 'From',
            'msg' : 'Message'
        }
        widgets = {
            'to' : forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Nama penerima...'}),
            'frm' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nama pengirim...'}),
            'title' :forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Judul pesan...'}),
            'msg' :forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Pesan yang ingin disampaikan... :D'})
        }