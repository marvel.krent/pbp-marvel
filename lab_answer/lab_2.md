**Apakah perbedaan antara JSON dan XML?**
JSON adalah format file yang menggunakan teks bersifat readable untuk menyimpan dan mentransmisikan objek data yang berisi pasangan *key* and *value*. 
XML adalah bahasa markup yang didesain untuk menyimpan data.
Perbedaan: 
- JSON memiliki tipe data yang jelas sedangkan XML tidak (*typeless*)
- JSON tidak mendukung namespace, sedangkan XML mendukung
- JSON tidak dapat menampilkan(*display*) data, sedangkan XML dapat 
- JSON hanya mendukung UTF-8 encoding format, sedangkan XML bervariasi 

**Apakah perbedaan antara HTML dan XML**
HTML adalah bahasa markup yang digunakan untuk membuat struktur dari halaman web statis
XML adalah bahasa markup yang dapat digunakan untuk membuat halaman web, tetapi bersifat dinamis karena digunakan untuk men*transport* data.
Perbedaan:
- HTML bersifat statis, XML bersifat dinamis
- HTML tidak case sensitive, XML case sensitive
- HTML tags adalah *predefined tags*, sedangkan XML tags adalah *user defined tags*
- HTML tags digunakan untuk menampilkan data, sedangkan XML tags digunakan untuk mendeskripsikan data (bukan menampilkan)
- HTML tidak menyimpan data (hanya menampilkan), sedangkan XML membawa data dari *database* dan sebaliknya
