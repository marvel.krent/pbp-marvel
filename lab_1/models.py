from django.db import models
import datetime
# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30, default = "default_name")
    # TODO Implement missing attributes in Friend model
    npm  = models.CharField(max_length = 10, default = "default_npm")
    dob = models.DateField(null = True)
