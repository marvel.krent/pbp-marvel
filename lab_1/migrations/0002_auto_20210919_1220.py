# Generated by Django 3.2.7 on 2021-09-19 05:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='friend',
            name='dob',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='friend',
            name='npm',
            field=models.CharField(default='default_npm', max_length=10),
        ),
        migrations.AlterField(
            model_name='friend',
            name='name',
            field=models.CharField(default='default_name', max_length=30),
        ),
    ]
