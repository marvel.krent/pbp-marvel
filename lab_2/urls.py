from django.urls import path, re_path, include
from .views import index, xml, json

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json'),
]
