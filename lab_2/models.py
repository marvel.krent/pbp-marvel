from django.db import models

# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30, default = "")
    frm = models.CharField(max_length=30, default = "")
    title = models.CharField(max_length=30, default = "")
    msg = models.TextField()
