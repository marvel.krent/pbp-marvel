import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_complete_guide/widgets/profile.dart';

class ProfileScreen extends StatelessWidget{
  static String routeName = '/profile';

  @override
  Widget build(BuildContext context){
    return Column(
      children: [
        SizedBox(height: 20,),
        ProfilePic(),
        SizedBox(height: 20,),
        Choices(
            text: "My Account",
            icon: "assets/images/user.png",
            press: (){}
            ),
        Choices(
            text: "Billings",
            icon: "assets/images/billings.png",
            press: (){}
        ),
        Choices(
            text: "Notifications",
            icon: "assets/images/notification.png",
            press: (){}
        ),
        Choices(
            text: "Security & Privacy",
            icon: "assets/images/shield.png",
            press: (){}
        ),
      ],
    );
  }
}

class Choices extends StatelessWidget{
  const Choices({
    Key key,
    @required this.text,
    @required this.icon,
    @required this.press,
}) : super(key:key);

  final String text, icon;
  final VoidCallback press;

  @override
  Widget build(BuildContext context){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: FlatButton(
          padding: EdgeInsets.all(20),
          color: Theme.of(context).accentColor,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          onPressed: press,
          child: Row(
            children: [
              Image.asset(
                icon,
                width: 22,
              ),
              SizedBox(width:20),
              Expanded(
                child: Text(
                  text,
                  style: Theme.of(context).textTheme.bodyText2,
                ),
              ),
              Icon(Icons.arrow_forward_ios)
            ],
          )
      ),
    );
  }
}