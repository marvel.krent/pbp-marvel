import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Customize Mask",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}


class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();

  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;
  String? dropdownValue;
  String? dropdownValueSize;
  String? dropdownValueModel;
  String? dropdownValueColor;
  String? namaMasker;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Customize Mask"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Image.asset(
                  "assets/images/maskers3.png",
                  height: 250,

                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: new InputDecoration(
                      hintText: "contoh: Masker PBP Yahuu",
                      labelText: "Berikan Nama Maskermu!",
                      icon: Icon(Icons.edit_outlined),
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      namaMasker = value;
                    },
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                      hintText: "Sex",
                      labelText: "Sex",
                      icon: Icon(Icons.wc_outlined),
                    ),
                    value: dropdownValue,
                    icon: const Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: const TextStyle(color: Colors.blue),

                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownValue = newValue!;
                      });
                    },
                      validator: (value) => value == null ? 'Sex tidak boleh kosong!' : null,
                    items: <String>['Man', 'Woman', 'Unisex']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                      hintText: "Size",
                      labelText: "Size",
                      icon: Icon(Icons.format_size_outlined),
                    ),
                    value: dropdownValueSize,
                    icon: const Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: const TextStyle(color: Colors.blue),

                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownValueSize = newValue!;
                      });
                    },
                    validator: (value) => value == null ? 'Size tidak boleh kosong!' : null,
                    items: <String>['XL', 'L', 'M', 'S']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                      hintText: "Model",
                      labelText: "Model",
                      icon: Icon(Icons.masks_outlined),
                    ),
                    value: dropdownValueModel,
                    icon: const Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: const TextStyle(color: Colors.blue),

                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownValueModel = newValue!;
                      });
                    },
                    validator: (value) => value == null ? 'Model tidak boleh kosong!' : null,
                    items: <String>['Surgical', 'Sponge', 'Pitta', 'Cloth']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: DropdownButtonFormField<String>(
                    decoration: InputDecoration(
                      hintText: "Color",
                      labelText: "Color",
                      icon: Icon(Icons.palette_outlined),
                    ),
                    value: dropdownValueColor,
                    icon: const Icon(Icons.arrow_downward),
                    iconSize: 24,
                    elevation: 16,
                    style: const TextStyle(color: Colors.blue),

                    onChanged: (String? newValue) {
                      setState(() {
                        dropdownValueColor = newValue!;
                      });
                    },
                    validator: (value) => value == null ? 'Warna tidak boleh kosong!' : null,
                    items: <String>['Red', 'Green', 'Blue', 'Black']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                ),


                CheckboxListTile(
                  title: Text('Mask Connector'),
                  subtitle: Text('Biasanya digunakan untuk wanita berhijab'),
                  value: nilaiCheckBox,
                  activeColor: Colors.blue,
                  onChanged: (value) {
                    setState(() {
                      nilaiCheckBox = value!;
                    });
                  },
                ),
                // SwitchListTile(
                //   title: Text('Backend Programming'),
                //   subtitle: Text('Dart, Nodejs, PHP, Java, dll'),
                //   value: nilaiSwitch,
                //   activeTrackColor: Colors.pink[100],
                //   activeColor: Colors.red,
                //   onChanged: (value) {
                //     setState(() {
                //       nilaiSwitch = value;
                //     });
                //   },
                // ),
                Slider(
                  value: nilaiSlider,
                  min: 0,
                  max: 100,
                  onChanged: (value) {
                    setState(() {
                      nilaiSlider = value;
                    });
                  },
                ),
                RaisedButton(
                  child: Text(
                    "Add to Chart",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text("Konfirmasi Pesananmu!"),
                            content: Text(
                                "Nama: " + namaMasker! + "\n" + "\n" +
                                "Sex: " + dropdownValue! + "\n" + "\n" +
                                "Size: " + dropdownValueSize! + "\n" + "\n" +
                                "Model: " + dropdownValueModel! + "\n" + "\n" +
                                "Color: " + dropdownValueColor! + "\n" + "\n" +
                                "Connector: " + nilaiCheckBox.toString()
                            ),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'Cancel'),
                                child: const Text('Cancel'),
                              ),
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'OK'),
                                child: const Text('OK'),
                              ),
                            ],
                          ));
                    }

                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

